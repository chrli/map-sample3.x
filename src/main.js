import Vue from 'vue'
import App from './App.vue'
import { loadScript,loadCss } from 'esri-loader'

Vue.config.productionTip = false

const options = {
  url: 'http://10.87.94.52/api331/arcgis_js_api/library/3.31/3.31/init.js',
};
loadScript(options)
loadCss("http://10.87.94.52/api331/arcgis_js_api/library/3.31/3.31/esri/themes/calcite/esri/esri.css")

new Vue({
  render: h => h(App),
}).$mount('#app')
