const testpoint=[{
    label: "青浦新城",
    value:10,
    x: -33000,
    y: -8160
}, {
    label: "嘉定新城",
    value:20,
    x: -20677,
    y: 10996
}, {
    label: "松江新城",
    value:30,
    x: -22986,
    y: -22330
}, {
    label: "奉贤新城",
    value:40,
    x: 2300,
    y: -35207
}, {
    label: "南汇新城",
    value:50,
    x: 42866,
    y: -36756
}, {
    label: "长三角示范区",
    value:70,
    x: -54412,
    y: -23749
}]

export default testpoint;